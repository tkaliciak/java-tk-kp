import apps.people.Member;
import apps.projects.Task;
import apps.projects.Project;
import apps.exceptions.FileExtensionException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

import java.io.File;
import java.io.PrintWriter;
import java.io.FileNotFoundException;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.*;

import java.sql.*;

class ProjectManager {

    public static void main(String[] args) throws FileNotFoundException, FileExtensionException {

        try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
        {
            System.out.println("Connection established");
            Statement statement = conn.createStatement();

            String initProject = "CREATE TABLE IF NOT EXISTS PROJECT(\n"
                    + " id INTEGER PRIMARY KEY,\n"
                    + " projectName     TEXT    NOT NULL);";

            String initTask = "CREATE TABLE IF NOT EXISTS TASK(\n"
                    + " id INTEGER PRIMARY KEY,\n"
                    + " taskName      TEXT    NOT NULL,\n"
                    + " deadline TEXT NOT NULL,\n"
                    + " projectID INTEGER,\n"
                    + " FOREIGN KEY (projectID) REFERENCES PROJECT(id));";

            String initMember = "CREATE TABLE IF NOT EXISTS MEMBER(\n"
                    + " id INTEGER PRIMARY KEY,\n"
                    + " firstName     TEXT    NOT NULL,\n"
                    + " lastName      TEXT    NOT NULL,\n"
                    + " email TEXT NOT NULL,\n"
                    + " projectID INTEGER,\n"
                    + " taskID INTEGER,\n"
                    + " FOREIGN KEY (projectID) REFERENCES PROJECT(id),\n"
                    + " FOREIGN KEY (taskID) REFERENCES TASK(id));";


            statement.execute(initProject);
            statement.execute(initTask);
            statement.execute(initMember);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MenuTableFrame frame = new MenuTableFrame();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setTitle("Project Manager");
                frame.setVisible(true);
            }
        });
    }
}

class MenuTableFrame extends JFrame {

    private static final int DEFAULT_WIDTH = 500;
    private static final int DEFAULT_HEIGHT = 300;
    private AddMember dialog = null;
    private AddTask dialogTask = null;
    private AddProject dialogProject = null;
    private AddPersonToProject dialogPersonToProject = null;
    private AddPersonToTask dialogPersonToTask = null;
    private AddTaskToProject dialogTaskToProject = null;
    private RemovePersonFromProject dialogRemovePersonFromProject = null;
    private RemovePersonFromTask dialogRemovePersonFromTask = null;
    private RemoveTaskFromProject dialogRemoveTaskFromProject = null;

    private MemberTableModel model;
    private TaskTableModel modelTask;
    private ProjectTableModel modelProject;
    private JTable table;
    private JTable tableTask;
    private JTable tableProject;
    private JFileChooser inFile;
    private JFileChooser outFile;

    public MenuTableFrame() {
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        model = new MemberTableModel();
        table = new JTable(model);
        modelTask = new TaskTableModel();
        tableTask = new JTable(modelTask);
        modelProject = new ProjectTableModel();
        tableProject = new JTable(modelProject);

        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem importItem = new JMenuItem("Import");
        inFile = new JFileChooser();
        outFile = new JFileChooser();
        importItem.addActionListener(event -> {
            inFile.setCurrentDirectory(new File("."));
            int res = inFile.showOpenDialog(MenuTableFrame.this);
            if(res == JFileChooser.APPROVE_OPTION)
            {
                Gson gson = new GsonBuilder().create();
                String name = inFile.getSelectedFile().getPath();
                String loadedLine = "";
                try(Scanner inf = new Scanner(Paths.get(name)))
                {
                    while (inf.hasNextLine()){
                        loadedLine = inf.nextLine();
                        if(loadedLine.contains("idProject")) break;
                        Member member = gson.fromJson(loadedLine, Member.class);
                        model.addMember(member);
                        model.fireTableDataChanged();
                    }
                    while (inf.hasNextLine()) {
                        if(!loadedLine.contains("idProject")) break;
                        Task task = gson.fromJson(loadedLine, Task.class);
                        modelTask.addTask(task);
                        modelTask.fireTableDataChanged();
                        loadedLine = inf.nextLine();
                    }
                    while(true) {
                        if(!loadedLine.contains("title")) break;
                        Project project= gson.fromJson(loadedLine, Project.class);
                        modelProject.addProject(project);
                        modelProject.fireTableDataChanged();
                        if(inf.hasNextLine()) {
                            loadedLine = inf.nextLine();
                        }
                        else break;
                    }
                }
                catch(IOException e)
                {
                    JOptionPane.showMessageDialog(MenuTableFrame.this, "Input file read failed!", "Import error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        JMenuItem exportItem = new JMenuItem("Export");
        exportItem.addActionListener(event -> {
            outFile.setCurrentDirectory(new File("."));
            int res = outFile.showSaveDialog(MenuTableFrame.this);
            if(res == JFileChooser.APPROVE_OPTION)
            {
                Gson gson = new GsonBuilder().create();
                String name = outFile.getSelectedFile().getPath();
                try(PrintWriter outf = new PrintWriter(name);)
                {
               for (Member m: model.getMembers()) {
                   String memberJson = gson.toJson(m);
                   outf.println(memberJson);
               }
                for(Task t: modelTask.getTasks()) {
                    String taskJson = gson.toJson(t);
                    outf.println(taskJson);
                }
               for (Project p: modelProject.getProjects()) {
                   String projectJson = gson.toJson(p);
                   outf.println(projectJson);
               }
                }
                catch(IOException e)
                {
                    JOptionPane.showMessageDialog(MenuTableFrame.this, "Output file read failed!", "Export error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.addActionListener(event -> System.exit(0));
        JMenuItem newItem = new JMenuItem("New");
        newItem.addActionListener(new NewMemberAction());
        fileMenu.add(newItem);
        fileMenu.addSeparator();
        fileMenu.add(importItem);
        fileMenu.add(exportItem);
        fileMenu.addSeparator();
        fileMenu.add(exitItem);

        // MenuBar -> Add
        JMenu addMenu = new JMenu("Add");
        JMenuItem addPersonItem = new JMenuItem("Add Person");
        addPersonItem.addActionListener(new NewMemberAction());
        addMenu.add(addPersonItem);

        JMenuItem addTaskItem = new JMenuItem("Add Task");
        addTaskItem.addActionListener(new NewTaskAction());
        addMenu.add(addTaskItem);

        JMenuItem addProjectItem = new JMenuItem("Add Project");
        addProjectItem.addActionListener(new NewProjectAction());
        addMenu.add(addProjectItem);


        JMenu aboutMenu = new JMenu("About");
        JMenuItem aboutItem = new JMenuItem("About ...");
        aboutItem.addActionListener(event -> {
            JDialog f = new SimpleAboutDialog(new JFrame());
            f.show();
        });
        aboutMenu.add(aboutItem);
        menuBar.add(fileMenu);
        menuBar.add(addMenu);
        menuBar.add(aboutMenu);
        setJMenuBar(menuBar);
        // dodwanie tabel
        add(new JScrollPane(table), BorderLayout.WEST);
        add(new JScrollPane(tableProject), BorderLayout.EAST);
        add(new JScrollPane(tableTask), BorderLayout.CENTER);
        // przyciski
        JButton APTPButton = new JButton("Add person to project");
        APTPButton.addActionListener(new APTPAction());

        JButton ATTPButton = new JButton("Add task to project");
        ATTPButton.addActionListener(new ATTPAction());

        JButton APTTButton = new JButton("Add person to task");
        APTTButton.addActionListener(new APTTAction());

        JButton RPFPButton = new JButton("Remove person from project");
        RPFPButton.addActionListener(new RPFPAction());

        JButton RTFPButton = new JButton("Remove task from project");
        RTFPButton.addActionListener(new RTFPAction());

        JButton RPFTButton = new JButton("Remove person from task");
        RPFTButton.addActionListener(new RPFTAction());

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(APTPButton);
        buttonsPanel.add(ATTPButton);
        buttonsPanel.add(APTTButton);
        buttonsPanel.add(RPFPButton);
        buttonsPanel.add(RTFPButton);
        buttonsPanel.add(RPFTButton);
        add(buttonsPanel, BorderLayout.SOUTH);
        pack();

        loadProjectsFromDb();
        loadTasksFromDb();
        loadMembersFromDb();
    }

    void loadProjectsFromDb() {
        try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
        {
            Statement statement = conn.createStatement();

            ResultSet rs = statement.executeQuery("select * from PROJECT");
            while(rs.next())
            {
                Project p = new Project(rs.getString("projectName"));
                modelProject.addProject(p);
                modelProject.fireTableDataChanged();
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    void loadTasksFromDb() {
        try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
        {
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("select * from TASK");
            while(rs.next())
            {
                String name = rs.getString("taskName");
                LocalDate deadline = LocalDate.parse(rs.getString("deadline"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                Task t = new Task(name,deadline);

                String projectID = rs.getString("projectID");
                if(projectID != null) {
                    t.setAttr(3,projectID);
                }

                modelTask.addTask(t);
                modelTask.fireTableDataChanged();
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    void loadMembersFromDb() {
        try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
        {
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("select * from MEMBER");
            while(rs.next())
            {
                Member m = new Member(rs.getString("firstName"),
                                      rs.getString("lastName"),
                                      rs.getString("email"));

                String projectID = rs.getString("projectID");
                String taskID = rs.getString("taskID");

                if(projectID != null) {
                    m.setAttr(4,projectID);
                }
                if(taskID != null) {
                    m.setAttr(5,taskID);
                }

                model.addMember(m);
                model.fireTableDataChanged();
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    private class NewMemberAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if(dialog == null) dialog = new AddMember();

            if(dialog.showDialog(MenuTableFrame.this, "Add Member"))
            {
                Member p = dialog.getMember();
                model.addMember(p);
                try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
                {
                    Statement statement = conn.createStatement();
                    int row = model.getRowCount() - 1;
                    String text = String.format("insert into MEMBER(id, firstName, lastName, email)" +
                            " values (%s, '%s', '%s','%s')", model.getValueAt(row,0),
                            model.getValueAt(row,1),model.getValueAt(row,2),model.getValueAt(row,3));
                    statement.execute(text);

                }
                catch(SQLException e)
                {
                    e.printStackTrace();
                }
                model.fireTableDataChanged();
            }
        }
    }
    private class NewTaskAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if (dialogTask == null) dialogTask = new AddTask();

            if (dialogTask.showDialog(MenuTableFrame.this, "Add Task")) {
                ArrayList<String> params = dialogTask.getData();
                int projectId = Integer.parseInt(params.get(1));
                String taskName = params.get(0);
                LocalDate inputDate;
                try {
                    inputDate = LocalDate.parse(params.get(2), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                } catch (DateTimeParseException e) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Wrong data format",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (modelProject.getRowCount() < projectId) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Project not found",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    Task t = new Task(taskName, inputDate);
                    modelTask.addTask(t);
                    modelTask.setValueAt(params.get(1),Integer.parseInt(t.getId())-1,3);

                    try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
                    {
                        Statement statement = conn.createStatement();
                        int row = modelTask.getRowCount() - 1;
                        String text = String.format("insert into TASK(id, taskName,deadline,projectID)" +
                                        " values (%s, '%s', '%s', %s)", modelTask.getValueAt(row,0),
                                        modelTask.getValueAt(row,1),modelTask.getValueAt(row,2),
                                        modelTask.getValueAt(row,3));
                        statement.execute(text);
                    }
                    catch(SQLException e)
                    {
                        e.printStackTrace();
                    }
                    modelTask.fireTableDataChanged();
                }
            }
        }
    }
    private class NewProjectAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if(dialogProject == null) dialogProject = new AddProject();

            if(dialogProject.showDialog(MenuTableFrame.this, "Add Project")) {
                Project p = dialogProject.getProject();
                modelProject.addProject(p);
                try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
                {
                    Statement statement = conn.createStatement();
                    int row = modelProject.getRowCount() - 1;
                    String text = String.format("insert into PROJECT(id, projectName)" +
                                    " values (%s, '%s')", modelProject.getValueAt(row,0),
                                    modelProject.getValueAt(row,1));
                    statement.execute(text);

                }
                catch(SQLException e)
                {
                    e.printStackTrace();
                }
                modelProject.fireTableDataChanged();
            }
        }
    }

    private class APTPAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if(dialogPersonToProject == null) dialogPersonToProject = new AddPersonToProject();

            if(dialogPersonToProject.showDialog(MenuTableFrame.this, "Add Person to Project")) {
                ArrayList<Integer> params = dialogPersonToProject.getData();
                int personId = params.get(0);
                int projectId = params.get(1);
                if(model.getRowCount() < personId) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Person not found",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else if(modelProject.getRowCount() < projectId) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Project not found",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else {
                    model.setValueAt(projectId,personId-1,4);
                    try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
                    {
                        Statement statement = conn.createStatement();
                        String text = String.format("update MEMBER SET projectID = '%d' WHERE id = %s",
                                projectId,personId);
                        statement.execute(text);
                    }
                    catch(SQLException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private class APTTAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if(dialogPersonToTask== null) dialogPersonToTask = new AddPersonToTask();

            if(dialogPersonToTask.showDialog(MenuTableFrame.this, "Add Person to Task")) {
                ArrayList<Integer> params = dialogPersonToTask.getData();
                int personId = params.get(0);
                int taskId = params.get(1);
                if(model.getRowCount() < personId) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Person not found",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else if(modelTask.getRowCount() < taskId) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Task not found",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else {
                    model.setValueAt(taskId,personId-1,5);
                }
            }
        }
    }

    private class ATTPAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if(dialogTaskToProject== null) dialogTaskToProject = new AddTaskToProject();

            if(dialogTaskToProject.showDialog(MenuTableFrame.this, "Add Task to Project")) {
                ArrayList<Integer> params = dialogTaskToProject.getData();
                int taskId = params.get(0);
                int projectId = params.get(1);
                if(modelTask.getRowCount() < taskId) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Task not found",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else if(modelProject.getRowCount() < projectId) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Project not found",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else {
                    modelTask.setValueAt(projectId,taskId-1,3);
                }
            }
        }
    }

    private class RPFPAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if(dialogRemovePersonFromProject== null) dialogRemovePersonFromProject = new RemovePersonFromProject();

            if(dialogRemovePersonFromProject.showDialog(MenuTableFrame.this, "Remove person from Project")) {
                int personId = dialogRemovePersonFromProject.getData();
                if(model.getRowCount() < personId) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Person not found",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else {
                    model.setValueAt(0,personId-1,4);
                }
            }
        }
    }

    private class RPFTAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if(dialogRemovePersonFromTask== null) dialogRemovePersonFromTask = new RemovePersonFromTask();

            if(dialogRemovePersonFromTask.showDialog(MenuTableFrame.this, "Remove person from task")) {
                int personId = dialogRemovePersonFromTask.getData();
                if(model.getRowCount() < personId) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Person not found",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else {
                    model.setValueAt(0,personId-1,5);
                }
            }
        }
    }

    private class RTFPAction implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if(dialogRemoveTaskFromProject== null) dialogRemoveTaskFromProject = new RemoveTaskFromProject();

            if(dialogRemoveTaskFromProject.showDialog(MenuTableFrame.this, "Remove person from task")) {
                int taskId = dialogRemoveTaskFromProject.getData();
                if(modelTask.getRowCount() < taskId) {
                    JOptionPane.showMessageDialog(MenuTableFrame.this,
                            "Task not found",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else {
                    modelTask.setValueAt(0,taskId-1,3);
                }
            }
        }
    }
}

class MemberTableModel extends AbstractTableModel {

    private ArrayList<Member> people = new ArrayList<>();
    private static int columns = 6;
    private String[] columnNames = {"Id", "First name", "Last name", "Email", "Project", "Task"};

    public MemberTableModel() {
    }

    public int getRowCount() {
        return people.size();
    }

    public int getColumnCount() {
        return columns;
    }

    public Object getValueAt(int r, int c) {
        String value = people.get(r).attr(c);
        if (value.equals("0")) {
            return "None";
        } else {
            return value;
        }
    }

    public String getColumnName(int c) {
        return columnNames[c];
    }

    public void addMember(Member p) {
        people.add(p);
    }

    public ArrayList<Member> getMembers() {
        return people;
    }

    @Override
    public void setValueAt(Object value, int row, int col) { // edycja pola w wyswietlanej tabelii
        people.get(row).setAttr(col, String.valueOf(value));
        String column = "";
        switch (col){
            case 1:{ column = "firstName"; break;}
            case 2:{ column = "lastName"; break;}
            case 3:{ column = "email"; break;}
            case 4:{ column = "projectID"; break;}
            case 5:{ column = "taskID"; break;}
        } // row = id
        try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
        {
            int whichMember = row + 1;
            Statement statement = conn.createStatement();
            String text = String.format("UPDATE MEMBER" + "\n" +
                            "SET " + column + " = " + "'" + String.valueOf(value) + "'"+ "\n" +
                            "WHERE ID = " + whichMember  + ";");
            statement.execute(text);

        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        fireTableCellUpdated(row, col);
    }

    public boolean isCellEditable(int row, int col) {
        if (col > 0 && col < 4) {
            return true;
        } else {
            return false;
        }
    }
}


class TaskTableModel extends AbstractTableModel {

    private ArrayList<Task> tasks = new ArrayList<>();
    private static int columns = 4;
    private String[] columnNames = {"Id", "Task name", "Deadline", "Project"};

    public TaskTableModel() {
    }

    public int getRowCount() {
        return tasks.size();
    }

    public int getColumnCount() {
        return columns;
    }

    public Object getValueAt(int r, int c) {
        String value = tasks.get(r).attr(c);
        if(value.equals("0")) {
            return "None";
        }
        else {
            return value;
        }
    }

    public String getColumnName(int c) {
        return columnNames[c];
    }

    public void addTask(Task p) {
        tasks.add(p);
    }

    public ArrayList<Task> getTasks() {
        return tasks;
    }

    public void assigMemberToTask(int taskID, Member m) {
        tasks.get(taskID).assigTo(m);
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        tasks.get(row).setAttr(col,String.valueOf(value));
        String column = "";
        switch (col){
            case 1:{ column = "taskName"; break;}
            case 2:{ column = "deadline"; break;}
            case 3:{ column = "projectID"; break;}
        } // row = id
        try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
        {
            int whichTask = row + 1;
            Statement statement = conn.createStatement();
            String text = String.format("UPDATE TASK" + "\n" +
                    "SET " + column + " = " + "'" + String.valueOf(value) + "'"+ "\n" +
                    "WHERE ID = " + whichTask  + ";");
            statement.execute(text);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        fireTableCellUpdated(row, col);
    }

    public boolean isCellEditable(int row, int col) {
        if (col > 0 && col < 3) {
            return true;
        }
        else {
            return false;
        }
    }
}


class ProjectTableModel extends AbstractTableModel {

    private ArrayList<Project> project = new ArrayList<>();
    private static int columns = 2;
    private String[] columnNames = {"Id", "Project name"};

    public ProjectTableModel() {
    }

    public int getRowCount() {
        return project.size();
    }

    public int getColumnCount() {
        return columns;
    }

    public Object getValueAt(int r, int c) {
        return project.get(r).attr(c);
    }

    public String getColumnName(int c) {
        return columnNames[c];
    }

    public void addProject(Project p) {
        project.add(p);
    }

    public ArrayList<Project> getProjects() {
        return project;
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        project.get(row).setAttr(col,String.valueOf(value));
        String column = "";
        switch (col){
            case 1:{ column = "projectName"; break;}
        } // row = id
        try(Connection conn = DriverManager.getConnection("jdbc:sqlite:projectManager.db"))
        {
            int whichTask = row + 1;
            Statement statement = conn.createStatement();
            String text = String.format("UPDATE PROJECT" + "\n" +
                    "SET " + column + " = " + "'" + String.valueOf(value) + "'"+ "\n" +
                    "WHERE ID = " + whichTask  + ";");
            statement.execute(text);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }

        fireTableCellUpdated(row, col);
    }

    public boolean isCellEditable(int row, int col) {
        if (col != 0) {
            return true;
        }
        else {
            return false;
        }
    }}

class SimpleAboutDialog extends JDialog {
    public SimpleAboutDialog(JFrame parent) {
        super(parent, "About", true);

        Box b = Box.createVerticalBox();
        b.add(Box.createGlue());
        b.add(new JLabel("GUI ProjectManager"));
        b.add(new JLabel("Kaliciak Tomasz"));
        b.add(new JLabel("Popczyk Kamil"));
        b.add(new JLabel("2018 (c)"));
        b.add(Box.createGlue());
        getContentPane().add(b, "Center");

        JPanel p2 = new JPanel();
        JButton ok = new JButton("Ok");
        p2.add(ok);
        getContentPane().add(p2, "South");

        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                setVisible(false);
            }
        });

        setSize(250, 150);
    }
}