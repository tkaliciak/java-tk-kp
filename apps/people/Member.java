package apps.people;

import java.util.*;

public class Member
{
  private String firstName;
  private String lastName;
  private String email;
  private static int nextId = 1;
  private int id;
  private int projectId = 0;
  private int taskId = 0;

  public Member(String fn, String ln, String e)
  {
    firstName = fn;
    lastName = ln;
    email = e;
    id = nextId++;
  }


  public void modifyFn(String fn) {
    firstName = fn;
  }

  public void modifyLn(String ln) {
    lastName = ln;
  }

  public void modifyE(String e) {
    email = e;
  }

  @Override
  public String toString() {
    return firstName + " " + lastName + " (" + email + ")";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Member m = (Member) o;
    return Objects.equals(toString(), m.toString());
  }

  public String getId()
  {
    return Integer.toString(id);
  }

  interface GetAttr {
    String attr();
  }

  private transient GetAttr[] attrs = new GetAttr[] {
          new GetAttr() { public String attr() { return getId();} },
          new GetAttr() { public String attr() { return firstName;} },
          new GetAttr() { public String attr() { return lastName;} },
          new GetAttr() { public String attr() { return email;} },
          new GetAttr() { public String attr() { return Integer.toString(projectId);} },
          new GetAttr() { public String attr() { return Integer.toString(taskId);} },
  };

  public String attr(int index)
  {
    return attrs[index].attr();
  }

  public void setAttr(int index, String newValue) {
    if(index == 0) {
      id = Integer.parseInt(newValue);
    }
    else if(index == 1) {
      firstName = newValue;
    }
    else if(index == 2) {
      lastName = newValue;
    }
    else if(index == 3) {
      email = newValue;
    }
    else if(index == 4) {
      projectId = Integer.parseInt(newValue);
    }
    else if(index == 5) {
        taskId = Integer.parseInt(newValue);
    }
  }
    // do deserializacji, inaczej gson zainicjalizuje attrs nullem
    private Member() {
        attrs = new GetAttr[] {
                new GetAttr() { public String attr() { return getId();} },
                new GetAttr() { public String attr() { return firstName;} },
                new GetAttr() { public String attr() { return lastName;} },
                new GetAttr() { public String attr() { return email;} },
                new GetAttr() { public String attr() { return Integer.toString(projectId);} },
                new GetAttr() { public String attr() { return Integer.toString(taskId);} },
        };
        nextId++;
    }
}

