package apps.exceptions;

import java.io.IOException;

public class FileExtensionException extends IOException {
    public FileExtensionException(String msg) {
        super(msg);
    }
}
