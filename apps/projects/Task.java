package apps.projects;

import apps.people.Member;
import com.google.gson.annotations.Expose;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;


public class Task
        extends Base
        implements Delay, Comparable <Task>
{

  private Member executor;

  private LocalDate deadLine; // termin zakonczenia zadania

  private static int nextId = 1;
  private int id;
  private int idProject = 0;

  public Task(String t, LocalDate dL)
  {
    super(t);
    deadLine = dL;
    id = nextId++;
  }

  public Task(String t, Member e, LocalDate dL)
  {
    super(t);
    executor = e;
    id = nextId++;
  }

  public String getId()
    {
        return Integer.toString(id);
    }

  public void assigTo(Member to)
  {
    executor = to;
  }

  public Member getExecutor()
  {
    return executor;
  }

  public LocalDate getDeadLine() {return deadLine; }
  public void modifyDeadLine(LocalDate dL) {
      deadLine = dL;
    }

    @Override
    public String toString() {
    String out;
    if(executor == null) {
      out = "Task title: " + getTitle() + " Deadline: " + deadLine.toString();
    }
    else {
      out = "Task title: " + getTitle() + " Deadline: " + deadLine.toString() +
              " Executor: " + executor.toString();
    }
    return out;
  }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(getId(), task.getId());
    }

  @Override
  public int compareTo(Task other) {
      if( deadLine.isBefore(other.getDeadLine()) ) {
          return -1;
      }
      else if ( deadLine.isAfter(other.getDeadLine() )) {
          return 1;
      }
      else {
          return 0;
      }
  }

  @Override
  public int getDelay() {
      return LocalDate.now().until(deadLine).getDays();
  }

  @Override
  public boolean delayed() {
    int compareTime = this.getDelay();
    return compareTime < 0;
  }

  @Override
  public String getDescription() {
    return "ID " + getId() + " Task title: " + getTitle() +
            " Deadline: " + deadLine;
   }


    // do JTable
    interface GetAttr {
        String attr();
    }

    private transient GetAttr[] attrs = new GetAttr[] {
            new GetAttr() { public String attr() { return getId();} },
            new GetAttr() { public String attr() { return getTitle();} },
            new GetAttr() { public String attr() { return deadLine.toString();} },
            new GetAttr() { public String attr() { return Integer.toString(idProject);} }
    };

    public String attr(int index)
    {
        return attrs[index].attr();
    }

    public void setAttr(int index, String newValue) {
        if(index == 1) {
            modifyTitle(newValue);
        }
        else if(index == 2) {
            try {
                LocalDate temp = LocalDate.parse(newValue, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                deadLine = temp;
            }
            catch (DateTimeParseException e) {
                System.out.println("Error date format." + e.getParsedString());
            }
        }
        else if(index == 3) {
            idProject = Integer.parseInt(newValue);
        }
    }
    private Task()
    {
        super("");
        GetAttr[] attrs = new GetAttr[] {
                new GetAttr() { public String attr() { return getId();} },
                new GetAttr() { public String attr() { return getTitle();} },
                new GetAttr() { public String attr() { return deadLine.toString();} },
                new GetAttr() { public String attr() { return Integer.toString(idProject);} }
        };
        nextId++;
    }
}

