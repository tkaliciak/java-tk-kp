package apps.projects;

import apps.people.Member;

import java.util.*;

public class Project extends Base
{
  private transient ArrayList<Task> tasks;
  private transient ArrayList<Member> members;
  private static int nextId = 1;
  private int id;

  public Project(String t)
  {
    super(t);
    tasks = new ArrayList<>();
    members = new ArrayList<>();
    id = nextId++;
  }

  public String getId() {
    return Integer.toString(id);
  }

  public void addTask(Task t)
  {
    tasks.add(t);
  }

  public void addMember(Member m)
  {
    members.add(m);
  }

  public void showMembers() {
    for (Member member: members) {
      System.out.println(member.toString());
    }
  }

  public void showTasks() {
    for (Task task: tasks) {
      System.out.println(task.toString());
    }
  }

  public void deleteTask(Task t) {
    tasks.remove(t);
  }

  public ArrayList<Task> returnTasks() {
      return tasks;
}


  @Override
  public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Project p = (Project) o;
      return Objects.equals(getId(), p.getId());
  }

  @Override
  public String toString()
  {
    return "Project title: " + getTitle();
  }

  @Override
  public String getDescription() {
      return "ID " + getId() + " Project title: " + getTitle() +
              " Number of tasks: " + tasks.size() + " Number of members: "
              + members.size();
  }

  // do JTable
  interface GetAttr {
    String attr();
  }

  private transient GetAttr[] attrs = new GetAttr[] {
          new GetAttr() { public String attr() { return getId();} },
          new GetAttr() { public String attr() { return getTitle();} }
  };

  public String attr(int index)
  {
    return attrs[index].attr();
  }

  public void setAttr(int index, String newValue) {
    if(index == 1) {
      modifyTitle(newValue);
    }
  }

   private Project()
   {
       super("");
       GetAttr[] attrs = new GetAttr[] {
               new GetAttr() { public String attr() { return getId();} },
               new GetAttr() { public String attr() { return getTitle();} }
       };
        nextId++;
   }
}
