package apps.projects;

abstract class Base
{
  private String title;


  public Base(String t)
  {
    title = t;
  }

  public String getTitle()
  {
    return title;
  }

  public void modifyTitle(String t) {
    title = t;
  }

  public abstract String getDescription();

}
