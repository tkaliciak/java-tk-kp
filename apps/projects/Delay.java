package apps.projects;

public interface Delay {
    int getDelay();
    boolean delayed();
}