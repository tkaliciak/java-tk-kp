import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;

import javax.swing.*;

//import apps.people.Member;
import apps.projects.Task;

public class AddTask extends JPanel {
    private JTextField taskName;
    private JTextField projectID;
    private JTextField deadLine;
    private JButton saveButton;
    private JButton cancelButton;
    private boolean ok;
    private JDialog dialog;
    Frame owner = null;

    public AddTask() {
        setLayout(new BorderLayout());
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));
        panel.add(new JLabel("Task name:"));
        panel.add(taskName = new JTextField(""));
        panel.add(new JLabel("Project id:"));
        panel.add(projectID = new JTextField(""));
        panel.add(new JLabel("Deadline ( YYYY-MM-DD ):"));
        panel.add(deadLine = new JTextField(""));
        add(panel, BorderLayout.CENTER);

        saveButton = new JButton("Save");
        saveButton.addActionListener(event -> {
            ok = true;
            dialog.setVisible(false);
        });

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(event -> dialog.setVisible(false));

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(saveButton);
        buttonPanel.add(cancelButton);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    public ArrayList<String> getData() {
        ArrayList<String> params = new ArrayList<>();
        params.add(taskName.getText());
        params.add(projectID.getText());
        params.add(deadLine.getText());
        return params;
    }

    public boolean showDialog(Component parent, String title) {
        ok = false;

        if(parent instanceof Frame)
            owner = (Frame) parent;
        else
            owner = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, parent);

        if(dialog == null || dialog.getOwner() != owner)
        {
            dialog = new JDialog(owner, true);
            dialog.add(this);
            dialog.getRootPane().setDefaultButton(saveButton);
            dialog.pack();
        }

        dialog.setTitle(title);
        dialog.setVisible(true);
        return ok;
    }
}